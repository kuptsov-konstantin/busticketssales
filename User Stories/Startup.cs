﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(User_Stories.Startup))]
namespace User_Stories
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
